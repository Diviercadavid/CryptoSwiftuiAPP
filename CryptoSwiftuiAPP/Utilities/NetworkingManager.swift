//
//  NetworkingManager.swift
//  CryptoSwiftuiAPP
//
//  Created by Divier on 28/9/2023.
//

import Foundation
import Combine

class NetworkingManager {
    
    enum NetworkingError: LocalizedError {
        case badURLResponse(url: URL, data: Data)
        case unknown
        
        var errorDescription: String? {
            switch self {
            case .badURLResponse(url: let url, data: let data): 
                return "[🔥] Bad response from URL: \(url). \n \(String(data: data, encoding: String.Encoding.utf8) ?? "")"
            case .unknown: 
                return "<⚠️> Unknown error occurred"
            }
        }
    }
    
    static func download(url: URL) -> AnyPublisher<Data, any Error> {
        return  URLSession.shared.dataTaskPublisher(for: url)
//            .subscribe(on: DispatchQueue.global(qos: .default))
            .tryMap( {try handlerURLResponse(output: $0, url: url)} )
//            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    static func handlerURLResponse(output: URLSession.DataTaskPublisher.Output, url: URL) throws -> Data {
        guard let response = output.response as? HTTPURLResponse,
              response.statusCode >= 200 && response.statusCode < 300 else {
            throw NetworkingError.badURLResponse(url: url, data: output.data)
        }
        return output.data
        
    }
    
    static func handlerCompletion(completion: Subscribers.Completion<Error>) {
        
        if case let .failure(error) = completion {
            print("logging or analytics error \(error.localizedDescription)")
        }
        
    }
    
    static func handleCompletionError<T: Error>(_ error: T, onError: @escaping (Error) -> Void) {
        // Perform any logging or analytics here if needed.
        print("logging or analytics error \(error.localizedDescription)")
        onError(error)
    }
}
