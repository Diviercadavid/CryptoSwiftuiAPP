//
//  MarketDataModel.swift
//  CryptoSwiftuiAPP
//
//  Created by Divier on 3/10/2023.
//

import Foundation

//JSON data
/*
 URL : https://api.coingecko.com/api/v3/global
 
 JSON Response:
 {
   "data": {
     "active_cryptocurrencies": 10244,
     "upcoming_icos": 0,
     "ongoing_icos": 49,
     "ended_icos": 3376,
     "markets": 876,
     "total_market_cap": {
       "btc": 41032479.786017194,
       "eth": 684063496.8292302,
       "ltc": 17474366681.406563,
       "bch": 4944270528.537359,
       "bnb": 5298029336.868164,
       "eos": 1951349078409.5874,
       "xrp": 2089993558325.6538,
       "xlm": 9830523717544.07,
       "link": 151708786424.63846,
       "dot": 279852319199.90564,
       "yfi": 218566508.1547281,
       "usd": 1121512220700.454,
       "aed": 4119314386632.768,
       "ars": 392555520631123.2,
       "aud": 1780999537887.823,
       "bdt": 123702114942317.77,
       "bhd": 422755153105.2583,
       "bmd": 1121512220700.454,
       "brl": 5795750854135.811,
       "cad": 1537935315807.6357,
       "chf": 1033622672500.8193,
       "clp": 1029099613714737.2,
       "cny": 8070177637716.343,
       "czk": 26268844291800.523,
       "dkk": 7992840398001.272,
       "eur": 1071694647856.9403,
       "gbp": 929397177294.4645,
       "hkd": 8782860522555.968,
       "huf": 417860588910016.9,
       "idr": 17531634944375378,
       "ils": 4314645927087.743,
       "inr": 93349009128342.44,
       "jpy": 167334109377390.47,
       "krw": 1526935070856069.2,
       "kwd": 346794008884.9944,
       "lkr": 363423497135826.44,
       "mmk": 2356195400700650.5,
       "mxn": 20272029848249.754,
       "myr": 5305874316133.859,
       "ngn": 859078361056547.1,
       "nok": 12309123732931.22,
       "nzd": 1907166298179.965,
       "php": 63676941610387.57,
       "pkr": 316396490065566.9,
       "pln": 4964858338209.889,
       "rub": 111231578684534.33,
       "sar": 4206514204816.6606,
       "sek": 12449708654332.66,
       "sgd": 1541916684191.1216,
       "thb": 41671468828456.266,
       "try": 30907153038221.285,
       "twd": 36357743926777.67,
       "uah": 41120257908616.57,
       "vef": 112297018658.73637,
       "vnd": 27357303031805030,
       "zar": 21705298914548.285,
       "xdr": 853259955655.5524,
       "xag": 53348183035.256996,
       "xau": 615519552.0870315,
       "bits": 41032479786017.195,
       "sats": 4103247978601719.5
     },
     "total_volume": {
       "btc": 1421550.077884975,
       "eth": 23699043.3497329,
       "ltc": 605390837.8554584,
       "bch": 171291820.32331884,
       "bnb": 183547620.2203195,
       "eos": 67603528932.68737,
       "xrp": 72406798738.72452,
       "xlm": 340573658457.9283,
       "link": 5255876278.55901,
       "dot": 9695345936.427776,
       "yfi": 7572129.15989251,
       "usd": 38854239202.69554,
       "aed": 142711620591.50073,
       "ars": 13599892910140.78,
       "aud": 61701852898.01334,
       "bdt": 4285598921825.647,
       "bhd": 14646144321.695337,
       "bmd": 38854239202.69554,
       "brl": 200790937351.69016,
       "cad": 53281012489.852394,
       "chf": 35809349039.097824,
       "clp": 35652649892393.445,
       "cny": 279587334454.7571,
       "czk": 910071188572.9768,
       "dkk": 276908023827.8173,
       "eur": 37128333897.31182,
       "gbp": 32198508027.27373,
       "hkd": 304277882423.9369,
       "huf": 14476574552837.996,
       "idr": 607374868655161.6,
       "ils": 149478785724.95645,
       "inr": 3234030501907.4487,
       "jpy": 5797207905998.982,
       "krw": 52899914414639.74,
       "kwd": 12014507846.257515,
       "lkr": 12590628286490.166,
       "mmk": 81629230620364.19,
       "mxn": 702314502071.2665,
       "myr": 183819405667.953,
       "ngn": 29762347229264.76,
       "nok": 426443536742.0091,
       "nzd": 66072838245.59909,
       "php": 2206056318745.091,
       "pkr": 10961400759612.643,
       "pln": 172005074862.0669,
       "rub": 3853563327560.6245,
       "sar": 145732615397.9884,
       "sek": 431314032188.78375,
       "sgd": 53418945039.021935,
       "thb": 1443687538934.9514,
       "try": 1070763113460.6342,
       "twd": 1259596153592.5854,
       "uah": 1424591107763.4631,
       "vef": 3890474971.365901,
       "vnd": 947780306196310.6,
       "zar": 751969403833.2876,
       "xdr": 29560771436.281147,
       "xag": 1848221558.7329628,
       "xau": 21324372.10161544,
       "bits": 1421550077884.9749,
       "sats": 142155007788497.5
     },
     "market_cap_percentage": {
       "btc": 47.531553300481356,
       "eth": 17.699893795969025,
       "usdt": 7.446564686820197,
       "bnb": 2.9096246714962817,
       "xrp": 2.5150867978921254,
       "usdc": 2.261031775993155,
       "steth": 1.2942934095614806,
       "sol": 0.8647556871320747,
       "ada": 0.8130096066798114,
       "doge": 0.7697897143695556
     },
     "market_cap_change_percentage_24h_usd": -0.6299596601147357,
     "updated_at": 1696385313
   }
 }
 
 */

// MARK: - Welcome
struct GlobalData: Codable {
    let data: MarketDataModel?
}

// MARK: - DataClass
struct MarketDataModel: Codable {
    let totalMarketCap, totalVolume, marketCapPercentage: [String: Double]?
    let marketCapChangePercentage24HUsd: Double?

    enum CodingKeys: String, CodingKey {
        
        case totalMarketCap = "total_market_cap"
        case totalVolume = "total_volume"
        case marketCapPercentage = "market_cap_percentage"
        case marketCapChangePercentage24HUsd = "market_cap_change_percentage_24h_usd"
    }
    
    var marketCap: String {
        if let item = totalMarketCap?.first(where: {$0.key == "usd"}) {
            return "$\(item.value.formattedWithAbbreviations())"
        }
        return ""
    }
    
    var volume: String {
        if let item = totalVolume?.first(where: {$0.key == "usd"}) {
            return "$\(item.value.formattedWithAbbreviations())"
        }
        return ""
    }
    
    var btcDominance: String {
        if let item = marketCapPercentage?.first(where: {$0.key == "btc"}) {
            return "\(item.value.asPercentString())"
        }
        return ""
    }
}

