//
//  HomeView.swift
//  CryptoSwiftuiAPP
//
//  Created by Divier on 27/9/2023.
//

import SwiftUI

struct HomeView: View {
    
    @State private var showPortfolio: Bool = false
    @State private var showPortfolioView: Bool = false
    @State private var selectedCoin: CoinModel? = nil
    @State private var showDetailView: Bool = false
    @State private var showSettingsView: Bool = false
    
    @EnvironmentObject private var homeViewModel: HomeViewModel
    
    
    var body: some View {
        ZStack {
            Color.theme.background.ignoresSafeArea()
                .sheet(isPresented: $showPortfolioView, content: {
                    PortfolioView()
                        .environmentObject(homeViewModel)
                })
            
            VStack {
                
                homeHeader
                
                HomeStatsView(showPortfolio: $showPortfolio)
                
                SearchBarView(searchText: $homeViewModel.searchText)
                
                columnTitles
                    .padding(.horizontal)
                    .font(.caption)
                    .foregroundStyle(Color.theme.secondaryText)
                
                if !showPortfolio {
                    allCoinsList
                        .transition(.move(edge: .leading))
                }
                
                if showPortfolio {
                    
                    ZStack {
                        if homeViewModel.portfolioCoins.isEmpty && homeViewModel.searchText.isEmpty {
                            portfolioEmptyText
                            
                        } else {
                            portfolioCoinsList
                                
                        }
                    }
                    .transition(.move(edge: .trailing))
                    
                    
                }
                
                
                Spacer()
            }
            .sheet(isPresented: $showSettingsView, content: {
                SettingsView()
            })
            .alert(item: $homeViewModel.errorObject) { returnedError in
                Alert(title: Text("Error"), message: Text(returnedError.error?.localizedDescription ?? ""), dismissButton: .default(Text("OK")))
                        }
            .navigationDestination(isPresented: $showDetailView, destination: {
                DetailLoadingView(coin: $selectedCoin)
            })
            
        }

    }
}

extension HomeView {
    
    private var homeHeader: some View {
        HStack {
            CircleButtonView(iconeName: showPortfolio ? "plus" : "info")
                .animation(.none, value: 0)
                .onTapGesture {
                    if showPortfolio {
                        showPortfolioView.toggle()
                    } else {
                        showSettingsView.toggle()
                    }
                }
                .background(CircleButtonAnimationView(animate: $showPortfolio))
            Spacer()
            
            Text(showPortfolio ? "Portfolio" : "Live Prices")
                .animation(.none)
                .font(.headline)
                .fontWeight(.heavy)
                .foregroundColor(Color.theme.accent)
                .animation(.none, value: 0)
            Spacer()
            CircleButtonView(iconeName: "chevron.right")
                .rotationEffect(Angle(degrees: showPortfolio ? 180 : 0))
                .onTapGesture {
                    withAnimation {
                        showPortfolio.toggle()
                    }
                }
        }
        .padding(.horizontal)
    }
    
    private var allCoinsList: some View {
        List {
            ForEach(homeViewModel.allCoins) { item in
                CoinRowView(coin: item, showHoldingsColumn: false)
                    .listRowInsets(.init(top: 10, leading: 0, bottom: 10, trailing: 10))
                    .onTapGesture {
                        segue(coin: item)
                    }
                    .listRowBackground(Color.theme.background)
            }
        }
        .refreshable {
            homeViewModel.reloadData()
        }
        .overlay(content: {
            loadingView
        })
        .listStyle(.plain)
    }
    
    private var portfolioCoinsList: some View {
        List {
            ForEach(homeViewModel.portfolioCoins) { item in
                CoinRowView(coin: item, showHoldingsColumn: true)
                    .listRowInsets(.init(top: 10, leading: 0, bottom: 10, trailing: 10))
                    .onTapGesture {
                        segue(coin: item)
                    }
                    .listRowBackground(Color.theme.background)
            }
        }
        .listStyle(.plain)
    }
    
    private var portfolioEmptyText:some View {
        
        Text("You haven't added any coins to your portfolio yet! Click the + button to get started 😁")
            .font(.callout)
            .foregroundStyle(Color.theme.accent)
            .fontWeight(.medium)
            .multilineTextAlignment(.center)
            .padding(50)
    }
    
    private var columnTitles: some View {
        HStack {
            
            HStack(spacing: 4) {
                Text("Coin")
                Image(systemName: "chevron.down")
                    .opacity((homeViewModel.sortOption == .rank || homeViewModel.sortOption == .rankReversed) ? 1.0 : 0.0)
                    .rotationEffect(Angle(degrees: homeViewModel.sortOption == .rank ? 0 : 180))
            }
            .onTapGesture {
                withAnimation {
                    homeViewModel.sortOption = (homeViewModel.sortOption == .rank) ? .rankReversed : .rank
                }
            }
            
            Spacer()
            if showPortfolio {
                HStack(spacing: 4) {
                    Text("Holdings")
                    Image(systemName: "chevron.down")
                        .opacity((homeViewModel.sortOption == .holdings || homeViewModel.sortOption == .holdingsReversed) ? 1.0 : 0.0)
                        .rotationEffect(Angle(degrees: homeViewModel.sortOption == .holdings ? 0 : 180))
                    
                }.onTapGesture {
                    withAnimation {
                        homeViewModel.sortOption = (homeViewModel.sortOption == .holdings) ? .holdingsReversed : .holdings
                    }
                }
                
            }
            HStack(spacing: 4) {
                Text("Price")
                    .frame(width: UIScreen.main.bounds.width / 3.5, alignment: .trailing)
                
                Image(systemName: "chevron.down")
                    .opacity((homeViewModel.sortOption == .price || homeViewModel.sortOption == .priceReversed) ? 1.0 : 0.0)
                    .rotationEffect(Angle(degrees: homeViewModel.sortOption == .price ? 0 : 180))
                
            }
            .onTapGesture {
                withAnimation {
                    homeViewModel.sortOption = (homeViewModel.sortOption == .price) ? .rankReversed : .price
                }
            }
            .frame(width: UIScreen.main.bounds.width / 3.5, alignment: .trailing)
            
        }
    }
    
    private var loadingView: some View {
        Group {
            if homeViewModel.isRefreshing {
                
                ProgressView()
                    .padding(30)
                    .progressViewStyle(CircularProgressViewStyle(tint: .gray))
                    .scaleEffect(2)
                    .background(RoundedRectangle(cornerRadius: 4)
                        .fill(Color.gray).opacity(0.3))
                    .offset(y: -40)
            }
        }
    }
}

extension HomeView {
    private func segue(coin: CoinModel) {
        print("going to \(coin.name)")
        showDetailView = true
        selectedCoin = coin
    }
}


#Preview {
    NavigationStack {
        HomeView().toolbar(.hidden)
            .environmentObject(DeveloperPreview.instance.homeVM)
    }
    
}
