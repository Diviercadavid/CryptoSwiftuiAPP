//
//  SettingsView.swift
//  CryptoSwiftuiAPP
//
//  Created by Divier on 17/10/2023.
//

import SwiftUI

struct SettingsView: View {
    
    let defaultURL = URL (string: "https://www.google.com")!
    let youtubeURL = URL (string: "https://www.youtube.com/c/swiftfulthinking")!
    let coffeeURL = URL (string: "https://www.buymeacoffee.com/nicksarno")!
    let coingeckoURL = URL (string: "https://www.coingecko.com")!
    let personalURL = URL (string: "https://www.linkedin.com/in/diviercadavid/")!
    
    
    var body: some View {
        NavigationStack {
            
            ZStack {
                List {
                    
                    swiftfulThinkingSection
                        .listRowBackground(Color.theme.background)
                    
                    coinGeckoSection
                        .listRowBackground(Color.theme.background)
                    
                    developerSection
                        .listRowBackground(Color.theme.background)
                    
                    applicationSection
                        .listRowBackground(Color.theme.background)
                }
                .background(Color.theme.background)
                .foregroundStyle(Color.blue)
                .navigationTitle("Settings")
                .toolbar {
                    ToolbarItem(placement: .topBarLeading) {
                        XMarkButton()
                    }
                }
            }
            .background(Color.theme.background
            )
        }
    }
}

extension SettingsView {
    
    private var swiftfulThinkingSection: some View {
        Section {
            VStack(alignment: .leading) {
                Image("logo")
                    .resizable()
                    .frame(width: 100, height: 100)
                    .clipShape(RoundedRectangle(cornerRadius: 20))
                Text("This app was made by following a @SwiftfulThinking course on youtube. It uses MVVM Architecture, Combine, and CoreData")
                    .font(.callout)
                    .fontWeight(.medium)
                    .foregroundStyle(Color.theme.accent)
            }
            .padding(.vertical)
            
            Link("Subscribe on Youtube 🔥", destination: youtubeURL)
            Link("Support his coffee addition ☕️ ", destination: coffeeURL)
                
        } header: {
            Text("Swiftful Thinking")
                .foregroundStyle(Color.theme.accent)
        }
    }
    
    private var coinGeckoSection: some View {
        Section {
            VStack(alignment: .leading) {
                Image("coingecko")
                    .resizable()
                    .scaledToFit()
                    .frame(height: 100)
                    .clipShape(RoundedRectangle(cornerRadius: 20))
                Text("The crypto currency data that is used in this app comes from a free API from CoinGecko! Pirces may be slightly delayed.")
                    .font(.callout)
                    .fontWeight(.medium)
                    .foregroundStyle(Color.theme.accent)
            }
            .padding(.vertical)
            
            Link("Visit coinGecko 🦎", destination: coingeckoURL)
                
        } header: {
            Text("CoinGecko")
                .foregroundStyle(Color.theme.accent)
        }
    }
    
    private var developerSection: some View {
        Section {
            VStack(alignment: .leading) {
                Image("logo")
                    .resizable()
                    .scaledToFit()
                    .frame(height: 100)
                    .clipShape(RoundedRectangle(cornerRadius: 20))
                Text("This app was developed by Divier Cadavid.It uses SwiftUI and is written 100% in swift. The project benefits from multi-threading, publishers/subscribers, and data persistence.")
                    .font(.callout)
                    .fontWeight(.medium)
                    .foregroundStyle(Color.theme.accent)
            }
            .padding(.vertical)
            
            Link("Visit LinkedIn ", destination: personalURL)
                
        } header: {
            Text("Developer")
                .foregroundStyle(Color.theme.accent)
        }
    }
    
    private var applicationSection: some View {
        Section {
            
            Link("Term of Service ", destination: defaultURL)
            Link("Privacy Policy ", destination: defaultURL)
            Link("Company Website ", destination: defaultURL)
            Link("Learn More ", destination: defaultURL)
                
        } header: {
            Text("Application")
                .foregroundStyle(Color.theme.accent)
        }
    }
}

#Preview {
    SettingsView()
}
